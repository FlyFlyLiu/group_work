/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author liufulai
 */
public class AccountDirectory {
    private ArrayList<UserAccount> accountDirectory;

    public AccountDirectory() {
        accountDirectory = new ArrayList<UserAccount>();
    }

    public ArrayList<UserAccount> getAccountDirectory() {
        return accountDirectory;
    }

    public void setAccountDirectory(ArrayList<UserAccount> accountDirectory) {
        this.accountDirectory = accountDirectory;
    }
    
    
    
    public UserAccount addAccount(){
        UserAccount ua = new UserAccount();
        this.accountDirectory.add(ua);
        return ua;
    }
    
    public void removeAccount(UserAccount ua){
        this.accountDirectory.remove(ua);
    }
    
    public UserAccount searchAccount(String keyword){
        for(UserAccount ua : this.accountDirectory){
            if(ua.getUserId().equalsIgnoreCase(keyword)){
                return ua;
            }
        }
        return null;
    }
    
    
}
