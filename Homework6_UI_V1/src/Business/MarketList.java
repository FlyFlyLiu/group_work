/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author liufulai
 */
public class MarketList {
    private ArrayList<Market> marketList;

    public MarketList() {
        marketList = new ArrayList<Market>();
    }

    public ArrayList<Market> getMarketList() {
        return marketList;
    }

    public void setMarketList(ArrayList<Market> marketList) {
        this.marketList = marketList;
    }
    
    public Market addMarket(){
        Market m = new Market();
        this.marketList.add(m);
        return m;
    }
    
    public void removeMarket(Market m){
        this.marketList.remove(m);
    }
    
    public Market searchMarket(String keyword){
        for(Market m : this.marketList){
            if(m.getMarketName().equals(keyword)){
                return m;
            }
        }
        return null;
    }
    
    
}
