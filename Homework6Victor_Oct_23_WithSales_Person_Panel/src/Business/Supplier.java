/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author liufulai
 */
public class Supplier {
    private String name;
    private ArrayList<Product> productCatalog;

    public Supplier() {
        productCatalog = new ArrayList<Product>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Product> getProductCatalog() {
        return productCatalog;
    }

    public void setProductCatalog(ArrayList<Product> productCatalog) {
        this.productCatalog = productCatalog;
    }
    
    public Product addProduct(){
        Product p = new Product();
        productCatalog.add(p);
        return p;
    }
    
    public void removeProduct(Product p){
        productCatalog.remove(p);
    }
    
    public Product searchProductByName(String keyword){
        for(Product p : productCatalog){
            if(p.getProductName().equalsIgnoreCase(keyword)){
                return p;
            }
        }
        return null;
    }
    
    public Product searchProductById(int keyword){
        for(Product p : productCatalog){
            if(p.getProductId()== keyword){
                return p;
            }
        }
        return null;
    }
    public String toString(){
        return this.name;
    }
    
    
    
    
}
