/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author liufulai
 */
public class Business {
    private SupplierDirectory supplierDirectory;
    private MarketList marketList;
    private AccountDirectory masterAccountDirectory;
    private PersonList masterPersonList;
    private MarketOfferList marketOfferList;

    public Business() {
        supplierDirectory = new SupplierDirectory();
        marketList = new MarketList();
        masterAccountDirectory = new AccountDirectory();
        masterPersonList = new PersonList();
        marketOfferList = new MarketOfferList();
    }
    
    

    public SupplierDirectory getSupplierDirectory() {
        return supplierDirectory;
    }

    public void setSupplierDirectory(SupplierDirectory supplierDirectory) {
        this.supplierDirectory = supplierDirectory;
    }

    public MarketList getMarketList() {
        return marketList;
    }

    public void setMarketList(MarketList marketList) {
        this.marketList = marketList;
    }

    public AccountDirectory getMasterAccountDirectory() {
        return masterAccountDirectory;
    }

    public void setMasterAccountDirectory(AccountDirectory masterAccountDirectory) {
        this.masterAccountDirectory = masterAccountDirectory;
    }

    public PersonList getMasterPersonList() {
        return masterPersonList;
    }

    public void setMasterPersonList(PersonList masterPersonList) {
        this.masterPersonList = masterPersonList;
    }

    public MarketOfferList getMarketOfferList() {
        return marketOfferList;
    }

    public void setMarketOfferList(MarketOfferList marketOfferList) {
        this.marketOfferList = marketOfferList;
    }
    
    public int refershMasterAccountDirectory(){
        ArrayList<UserAccount> copy = (ArrayList<UserAccount>)this.masterAccountDirectory.getAccountDirectory().clone();
        for(UserAccount us : copy){
            this.masterAccountDirectory.removeAccount(us);
        }
        for(Person p : this.masterPersonList.getPersonDirectory()){
            if(p.getAccountDirectory().getAccountDirectory().size()>0){
                for(UserAccount us1 :p.getAccountDirectory().getAccountDirectory()){
                    this.masterAccountDirectory.getAccountDirectory().add(us1);
                }
            }
        }
        return this.masterAccountDirectory.getAccountDirectory().size();
    }
    
    public int initMakertOfferList(){
        for(Supplier s: this.supplierDirectory.getSupplierDirectory()){
            for(Product p: s.getProductCatalog()){
                for(Market m : this.getMarketList().getMarketList()){
                    MarketOffer mo = this.marketOfferList.addMarketOffer();
                    mo.setMarket(m);
                    mo.setProduct(p);
                }
            }
        }
        return this.masterAccountDirectory.getAccountDirectory().size();
    }
    
    public int UpdateMarketOfferListWhenSuppChange(Supplier s){
        for(Product p : s.getProductCatalog()){
            Boolean isNew = true;
            for(MarketOffer mo: this.getMarketOfferList().getMarketOfferList()){
                if(p.getProductName().equals(mo.getProduct().getProductName())){
                   isNew = false;
                }
            }
            if(isNew){
                for(Market m : this.getMarketList().getMarketList()){
                    MarketOffer mo = this.marketOfferList.addMarketOffer();
                    mo.setMarket(m);
                    mo.setProduct(p);
                }
            }
        }
        return this.masterAccountDirectory.getAccountDirectory().size();
    }
    
    public int UpdateMarketOfferListWhenMarketChange(Market m){
        for(Supplier s: this.getSupplierDirectory().getSupplierDirectory()){
            for(Product p : s.getProductCatalog()){
                MarketOffer mo = this.marketOfferList.addMarketOffer();
                mo.setMarket(m);
                mo.setProduct(p);
            }
        }
        return this.masterAccountDirectory.getAccountDirectory().size();
        
    }
    
    
   
}
