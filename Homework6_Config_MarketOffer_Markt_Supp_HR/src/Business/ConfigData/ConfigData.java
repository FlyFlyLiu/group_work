/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.ConfigData;

import Business.Business;

/**
 *
 * @author liufulai
 */
public class ConfigData {
    private Business business;

    public ConfigData() {
        business = new Business();
        new ConfigHRData(business);
        new ConfigSupplierData(business);
        new ConfigMarketData(business);
        new ConfigMarketOffer(business);
    }
    
    
}
