/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author liufulai
 */
public class OrderItem {
    private Product product;
    private int number;
    private int perDealPrice;

    public OrderItem() {
        product = new Product();
    }

    
    
    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getPerDealPrice() {
        return perDealPrice;
    }

    public void setPerDealPrice(int perDealPrice) {
        this.perDealPrice = perDealPrice;
    }
    
    public String toString(){
        return this.product.getProductName();
    }
    
}
