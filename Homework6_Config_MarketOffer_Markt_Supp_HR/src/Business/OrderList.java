/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author liufulai
 */
public class OrderList {
    private ArrayList<Order> orderList;

    public OrderList() {
        orderList = new ArrayList<Order>();
    }

    public ArrayList<Order> getOrderList() {
        return orderList;
    }

    public void setOrderList(ArrayList<Order> orderList) {
        this.orderList = orderList;
    }
    
    public Order addOrder(){
        Order o = new Order();
        this.orderList.add(o);
        return o;
    }
    
    public void removeOrder(Order o){
        this.orderList.remove(o);
    }
    
    public Order searchOrder(String keyword){
        for(Order o : this.orderList){
            if(o.getOrderId().equals(keyword))
                return o;
        }
        return null;
    }
    
    
}
