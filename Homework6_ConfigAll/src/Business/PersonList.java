/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author liufulai
 */
public class PersonList {
    private ArrayList<Person> personDirectory;

    public PersonList() {
        personDirectory = new ArrayList<Person>();
    }

    public ArrayList<Person> getPersonDirectory() {
        return personDirectory;
    }

    public void setPersonDirectory(ArrayList<Person> personDirectory) {
        this.personDirectory = personDirectory;
    }
    
    public Person addPerson(){
        Person p = new Person();
        this.personDirectory.add(p);
        return p;
    }
    
    public void removePerson(Person p){
        this.personDirectory.remove(p);
    }
    
    public Person searchPeron(String keyword){
        for(Person p : this.personDirectory){
            if(p.getName().equalsIgnoreCase(keyword)){
                return p;
            }
        }
        return null;
    }
    
    
    
    
}
