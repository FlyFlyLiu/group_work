/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author liufulai
 */
public class Market {
    private String marketName;
    private CustomerList customerList;
    private PersonList rankpersonList;

    public Market() {
        customerList = new CustomerList();
        rankpersonList = new PersonList();
       
    }

    public String getMarketName() {
        return marketName;
    }

    public void setMarketName(String marketName) {
        this.marketName = marketName;
    }

    public CustomerList getCustomerList() {
        return customerList;
    }

    public void setCustomerList(CustomerList customerList) {
        this.customerList = customerList;
    }

    public PersonList getRankpersonList() {
        return rankpersonList;
    }

    public void setRankpersonList(PersonList rankpersonList) {
        this.rankpersonList = rankpersonList;
    }
    
    public String toString(){
        return this.marketName;
    }
    
    public int customerNum(){
        return this.getCustomerList().getCustomerList().size();
    }
    
    
    
}
