/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.ConfigData;
import Business.*;
import java.util.Random;
/**
 *
 * @author liufulai
 */
public class ConfigMarketOffer {

    
    private Business business;
    private MarketOfferList marketOfferList;
   
    public ConfigMarketOffer(Business business){
        this.business = business;
        this.marketOfferList = business.getMarketOfferList();
        ScanAll();
        
    }
    
    public void ScanAll(){
        
        for(Market m: this.business.getMarketList().getMarketList()){
            for(Supplier s: this.business.getSupplierDirectory().getSupplierDirectory()){
                for(Product p: s.getProductCatalog()){
                MarketOffer mk = this.business.getMarketOfferList().addMarketOffer();
                mk.setMarket(m);
                mk.setProduct(p);
                if(Hit(m,s)){
                    //General Benefit Offer
                    mk.setFloorPrice(MarketLowPrice(p));
                    mk.setCellingPrice(MarketHighPrice(p));
                }
                else{
                    //General Common Offer
                    mk.setFloorPrice(CommonLowPrice(p));
                    mk.setCellingPrice(CommonHighPrice(p));
                }
                }
            }
        }
        
        int CountProduct = 0;
        for(Supplier s : this.business.getSupplierDirectory().getSupplierDirectory()){
            for(Product p : s.getProductCatalog()){
                CountProduct++;
            }
        }
        int CountMarket = this.business.getMarketList().getMarketList().size();
        int CountOffer = this.business.getMarketOfferList().getMarketOfferList().size();
        System.out.println("Markt Offer Config : [Product]: "+CountProduct+" [Market]: "+CountMarket+" [CountOffer:] "+CountOffer);
        
    }
    
    public int CommonLowPrice(Product p){
        Random r = new Random();
        int floorPrice = p.getTargetPrice()/2;
        int cielingPrice = p.getTargetPrice();
        int result = r.nextInt(cielingPrice - floorPrice) + floorPrice;
        return result;
    }
    
    public int MarketLowPrice(Product p){
        Random r = new Random();
        int floorPrice = 0;
        int cielingPrice = p.getTargetPrice();
        int result = r.nextInt(cielingPrice - floorPrice) + floorPrice;
        return result;
    }
    
    public int MarketHighPrice(Product p){
        Random r = new Random();
        int floorPrice = p.getTargetPrice();
        int cielingPrice = p.getTargetPrice()*2;
        int result = r.nextInt(cielingPrice - floorPrice) + floorPrice;
        return result;
    }
    
    public int CommonHighPrice(Product p){
        Random r = new Random();
        int floorPrice = p.getTargetPrice();
        int cielingPrice = p.getTargetPrice() + p.getTargetPrice()/2;
        if((cielingPrice - floorPrice)<1){
            return 1;
        }
        else{
            int result = r.nextInt(cielingPrice - floorPrice) + floorPrice;
            return result;
        }
    }
    
    
    
    public Boolean Hit(Market m, Supplier s){
        Boolean tag = false;
        if((m.getMarketName().equalsIgnoreCase("House Wife"))&&(s.getName().equals("Food Supplier"))){
            tag = true;
        }
        else if((m.getMarketName().equalsIgnoreCase("NEU"))&&(s.getName().equals("Education Supplier"))){
            tag = true;
        }
        else if((m.getMarketName().equalsIgnoreCase("Home"))&&(s.getName().equals("Home Supplier"))){
            tag = true;
        }
        else if((m.getMarketName().equalsIgnoreCase("Makeup Girl"))&&(s.getName().equals("Makeup Supplier"))){
            tag = true;
        }
        return tag;
    }
    
    
    
}
