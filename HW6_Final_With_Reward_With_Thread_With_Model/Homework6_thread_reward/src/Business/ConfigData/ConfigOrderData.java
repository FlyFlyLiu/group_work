/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.ConfigData;
import Business.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
/**
 *
 * @author liufulai
 */
public class ConfigOrderData {
    private Business business;
    private MarketList marketList;
    private PersonList masterPersonList;

    public ConfigOrderData(Business business) {
        this.business = business;
        this.marketList = this.business.getMarketList();
        this.masterPersonList = this.business.getMasterPersonList();
        ReadOrderCSV();
        ReadOrderItemCSV();
        
    }
    
   
    
    public void ReadOrderCSV(){
        File csv = new File("src/Business/ConfigData/Order.csv");
        BufferedReader br = null;
        try{
            br = new BufferedReader(new FileReader(csv));
        }
        catch(FileNotFoundException e){
            e.printStackTrace();
        }
        String line = "";
        String everyLine = "";
        try{
            ArrayList<String> allString = new ArrayList<>();
            
                while((line = br.readLine())!= null){
                everyLine = line;
                allString.add(everyLine);
                String [] splitStr = everyLine.split(",");
                for(Market m : this.business.getMarketList().getMarketList()){
                    for(Customer c: m.getCustomerList().getCustomerList()){
                        if(c.getName().equalsIgnoreCase(splitStr[0])){
                            Order o = c.getOrderList().addOrder();
                            o.setOrderId(splitStr[1]);
                            o.setDateStart(splitStr[3]);
                            o.setDateCompleted(splitStr[4]);
                            o.setStatus(splitStr[5]);
                            boolean tag = false;
                            for(Person p : this.business.getMasterPersonList().getPersonDirectory()){
                                if(p.getName().equalsIgnoreCase(splitStr[2])){
                                    o.setPerosn(p);
                                    //p.getOrderList().getOrderList().add(o);
                                    tag = true;
                                }
                            }
                            if(tag==false){
                                System.out.println("Order number:"+ o.getOrderId());
                            }
                        }
                    }
                }
            }
        }
        catch(IOException e){
            e.printStackTrace();
        }
        
        int Count1 = 0;
        for(Person p :this.business.getMasterPersonList().getPersonDirectory()){
            for(Order o : p.getOrderList().getOrderList()){
                Count1++;
            }
        }
        int Count2 = 0;
        for(Market m : this.business.getMarketList().getMarketList()){
            for(Customer c : m.getCustomerList().getCustomerList()){
                for(Order o : c.getOrderList().getOrderList()){
                    Count2++;
                }
            }
        }
        System.out.println("Order number from salse person:"+Count1);
        System.out.println("Order number from market customer:"+Count2);
        System.out.println("Config Order Successful: ");
    }
    
    public void ReadOrderItemCSV(){
        File csv = new File("src/Business/ConfigData/OrderItem.csv");
        BufferedReader br = null;
        try{
            br = new BufferedReader(new FileReader(csv));
        }
        catch(FileNotFoundException e){
            e.printStackTrace();
        }
        String line = "";
        String everyLine = "";
        try{
            ArrayList<String> allString = new ArrayList<>();
            
            while((line = br.readLine())!= null){
                everyLine = line;
                //System.out.println(everyLine);
                allString.add(everyLine);
                String [] splitStr = everyLine.split(",");
                for(Market m : this.business.getMarketList().getMarketList()){
                    for(Customer c : m.getCustomerList().getCustomerList()){
                        for(Order o : c.getOrderList().getOrderList()){
                            if(o.getOrderId().equals(splitStr[0])){
                                OrderItem oi = new OrderItem();
                                Product p  = FindProduct(splitStr[1]);
                                oi.setProduct(p);
                                oi.setNumber(Integer.parseInt(splitStr[2]));
                                int newAvail = p.getAvailNum() - oi.getNumber();
                                p.setAvailNum(newAvail);
                                oi.setPerDealPrice(SetPrice(m,p));
                                o.addOrderItem(oi);
                            }
                        }
                    }
                }
            }
        }
        catch(IOException e){
            e.printStackTrace();
        }
        
        int count = 0;
        for(Market m : this.business.getMarketList().getMarketList()){
            for(Customer c : m.getCustomerList().getCustomerList()){
                for(Order o : c.getOrderList().getOrderList()){
                    for(Person p : this.business.getMasterPersonList().getPersonDirectory()){
                        if(o.getPerosn().equals(p)){
                            p.getOrderList().getOrderList().add(o);
                        }
                    }
                    
                    int Price = o.GetOrderProfit();
                    o.setOrderPrice(Price);
                    if(o.getOrderPrice()==0){
                        System.out.println("Order Id:"+o.getOrderId());
                    }
                    
                    
                    count ++;
                }
            }
        }
        
        System.out.println("Config Order Item Successful! Order number: "+count);
    }
    
    public Product FindProduct(String keyword){
        for(Supplier s : this.business.getSupplierDirectory().getSupplierDirectory()){
            for(Product p : s.getProductCatalog()){
                if(p.getProductName().equalsIgnoreCase(keyword)){
                    return p;
                }
            }
        }
        return null;
    }
    
    public int SetPrice(Market m, Product p){
        for(MarketOffer mo : this.business.getMarketOfferList().getMarketOfferList()){
            if((mo.getProduct().equals(p))&&(mo.getMarket().equals(m))){
                 Random r = new Random();                 
                 int floorPrice = p.getTargetPrice();
                 int cielingPrice = mo.getCellingPrice();                
                 if(floorPrice<=cielingPrice){                                        
                     int result=(floorPrice+1);
                     return result;
                 }
                 else{
                    int result = r.nextInt(cielingPrice - floorPrice) + floorPrice;                   
                    return result;
                 }
                 
            }
        }
    
        return 0;
    }
    
    
    
    
}
