/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author liufulai
 */
public class CustomerList {
    private ArrayList<Customer> customerList;

    public CustomerList() {
        customerList = new ArrayList<Customer>();
    }

    public ArrayList<Customer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(ArrayList<Customer> customerList) {
        this.customerList = customerList;
    }
    
    public Customer addCustomer(){
        Customer c = new Customer();
        this.customerList.add(c);
        return c;
    }
    
    public void removeCustomer(Customer c){
        this.customerList.remove(c);
    }
    
    public Customer searchCustomer(String keyword){
        for(Customer c : this.customerList){
            if(c.getName().equalsIgnoreCase(keyword))
                return c;
        }
        return null;
    }
    
    
}
