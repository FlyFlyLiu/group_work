/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author liufulai
 */
public class SupplierDirectory {
    private ArrayList<Supplier> supplierDirectory;

    public SupplierDirectory() {
        supplierDirectory = new ArrayList<Supplier>();
    }

    public ArrayList<Supplier> getSupplierDirectory() {
        return supplierDirectory;
    }

    public void setSupplierDirectory(ArrayList<Supplier> supplierDirectory) {
        this.supplierDirectory = supplierDirectory;
    }
    
    public Supplier addSupplier(){
        Supplier s = new Supplier();
        this.supplierDirectory.add(s);
        return s;
    }
    
    public void removeProduct(Supplier s){
        this.supplierDirectory.remove(s);
    }
    
    public Supplier searchSupplier(String keyword){
        for(Supplier s : this.supplierDirectory){
            if(s.getName().equalsIgnoreCase(keyword)){
                return s;
            }
        }
        return null;
    }
    
    
}
