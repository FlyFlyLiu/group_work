/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author liufulai
 */
public class Order {
    private String OrderId;
    private ArrayList<OrderItem> orderItemList;
    private Person perosn;
    private String dateStart;
    private String dateCompleted;
    private String status;
    private int orderPrice;

    public Order() {
        orderItemList = new ArrayList<OrderItem>();
    }

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String OrderId) {
        this.OrderId = OrderId;
    }

    public ArrayList<OrderItem> getOrderItemList() {
        return orderItemList;
    }

    public void setOrderItemList(ArrayList<OrderItem> orderItemList) {
        this.orderItemList = orderItemList;
    }

    public Person getPerosn() {
        return perosn;
    }

    public void setPerosn(Person perosn) {
        this.perosn = perosn;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateCompleted() {
        return dateCompleted;
    }

    public void setDateCompleted(String dateCompleted) {
        this.dateCompleted = dateCompleted;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(int orderPrice) {
        this.orderPrice = orderPrice;
    }
    
    
    public void addOrderItem(OrderItem oi){
        this.orderItemList.add(oi);
    }
    
    public void remoeveOrderItem(OrderItem oi){
        this.orderItemList.remove(oi);
    }
    
    public OrderItem searchOrderItem(String keyword){
        for(OrderItem oi : this.orderItemList){
            if(oi.getProduct().getProductName().equalsIgnoreCase(keyword)){
                return oi;
            }
        }
        return null;
    }
    
    public String toString(){
        return this.OrderId;
    }
    
    public int GetOrderProfit(){
        int profit = 0;
        for(OrderItem oi : this.orderItemList){
            int perBalance = oi.getPerDealPrice() - oi.getProduct().getTargetPrice();
            //System.out.println("Balance: "+perBalance);
            int totalBalance = perBalance * oi.getNumber();
            profit = totalBalance + profit;
            //System.out.println("[Order]:"+this.getOrderId()+" [profit]:"+profit);
        }
        return profit;
    }
    
    public int GetOrderRevenue(){
        int Revenue = 0;
        for(OrderItem oi : this.orderItemList){
            int perRevenue = oi.getNumber()*oi.getPerDealPrice();
            Revenue = Revenue + perRevenue;
        }
        return Revenue;
    }
    
    
}
