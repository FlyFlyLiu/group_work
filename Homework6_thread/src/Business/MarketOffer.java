/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author liufulai
 */
public class MarketOffer {
    private Product product;
    private Market market;
    private int cellingPrice;
    private int floorPrice;

    public MarketOffer() {
        product = new Product();
        market = new Market();
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Market getMarket() {
        return market;
    }

    public void setMarket(Market market) {
        this.market = market;
    }

    public int getCellingPrice() {
        return cellingPrice;
    }

    public void setCellingPrice(int cellingPrice) {
        this.cellingPrice = cellingPrice;
    }

    public int getFloorPrice() {
        return floorPrice;
    }

    public void setFloorPrice(int floorPrice) {
        this.floorPrice = floorPrice;
    }

    public String toString(){
        return this.product.getProductName();
    }
    
    
    
}
