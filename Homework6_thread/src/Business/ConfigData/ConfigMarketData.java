/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.ConfigData;
import Business.*;
import Business.Business;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author liufulai
 */
public class ConfigMarketData {
    private Business business;
    private MarketList marketList;

    public ConfigMarketData(Business business) {
        this.business = business;
        this.marketList = business.getMarketList();
        ReadMarketCSV();
        ReadCustomerCSV();
    }
    
    public void ReadMarketCSV(){
        File csv = new File("src/Business/ConfigData/Market.csv");
        BufferedReader br = null;
        try{
            br = new BufferedReader(new FileReader(csv));
        }
        catch(FileNotFoundException e){
            e.printStackTrace();
        }
        String line = "";
        String everyLine = "";
        try{
            ArrayList<String> allString = new ArrayList<>();
            
                while((line = br.readLine())!= null){
                everyLine = line;
                //System.out.println(everyLine);
                allString.add(everyLine);
                String [] splitStr = everyLine.split(",");
                Market m = this.marketList.addMarket();
                m.setMarketName(splitStr[0]);
            }
        }
        catch(IOException e){
            e.printStackTrace();
        }
        int Count = business.getMarketList().getMarketList().size();
        System.out.println("Config Market Successful: "+Count);
    }
    
    public void ReadCustomerCSV(){
        File csv = new File("src/Business/ConfigData/Customer.csv");
        BufferedReader br = null;
        try{
            br = new BufferedReader(new FileReader(csv));
        }
        catch(FileNotFoundException e){
            e.printStackTrace();
        }
        String line = "";
        String everyLine = "";
        try{
            ArrayList<String> allString = new ArrayList<>();
            
            while((line = br.readLine())!= null){
                everyLine = line;
                //System.out.println(everyLine);
                allString.add(everyLine);
                String [] splitStr = everyLine.split(",");
                for(Market m : this.marketList.getMarketList()){
                    if(m.getMarketName().equals(splitStr[0])){
                        Customer c = m.getCustomerList().addCustomer();
                        c.setName(splitStr[1]);
                        c.setPhoneNumeber(splitStr[2]);
                        c.setType(splitStr[3]);
                    }
                }
            }
        }
        catch(IOException e){
            e.printStackTrace();
        }
        for(Market m : this.marketList.getMarketList()){
            int count = m.getCustomerList().getCustomerList().size();
            System.out.println(m.getMarketName()+": "+count);
        }
        System.out.println("Config Customer Successful");
    }
    
    
}
