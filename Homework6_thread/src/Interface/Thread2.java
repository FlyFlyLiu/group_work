/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;
import Business.*;

/**
 *
 * @author liufulai
 */
class Thread2 implements Runnable {
    private int name;
    private Business business;
    
    
    public Thread2(Business business,int name) {  
        this.business = business;
        this.name=name;  
        System.out.println("****************************");
        System.out.println("Thread ["+name+"] is running");
    }  
    
    public void run() {  
        new MainJFrame(business).setVisible(true);
          
    }  

    
}
