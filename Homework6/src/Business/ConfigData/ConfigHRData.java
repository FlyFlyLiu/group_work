/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.ConfigData;
import Business.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
/**
 *
 * @author liufulai
 */
public class ConfigHRData {
    private Business business;
    private PersonList masterPersonList;

    public ConfigHRData(Business business) {
        this.business = business;
        masterPersonList = business.getMasterPersonList();
        
        ReadPersonCSV();
        ReadAccountCSV();
        
        
    }
    
    public void ReadPersonCSV(){
        File csv = new File("src/Business/ConfigData/Person.csv");
        BufferedReader br = null;
        try{
            br = new BufferedReader(new FileReader(csv));
        }
        catch(FileNotFoundException e){
            e.printStackTrace();
        }
        String line = "";
        String everyLine = "";
        try{
            ArrayList<String> allString = new ArrayList<>();
            
                while((line = br.readLine())!= null){
                everyLine = line;
                //System.out.println(everyLine);
                allString.add(everyLine);
                String [] splitStr = everyLine.split(",");
                Person p = masterPersonList.addPerson();
                p.setName(splitStr[0]);
                p.setRevenue(0);
                //System.out.println(p.getName());
            }
        }
        catch(IOException e){
            e.printStackTrace();
        }
        int PersonCount = business.getMasterPersonList().getPersonDirectory().size();
        System.out.println("Config Person Successful: "+PersonCount);
    }
    
    public void ReadAccountCSV(){
        File csv = new File("src/Business/ConfigData/UserAccount.csv");
        BufferedReader br = null;
        try{
            br = new BufferedReader(new FileReader(csv));
        }
        catch(FileNotFoundException e){
            e.printStackTrace();
        }
        String line = "";
        String everyLine = "";
        try{
            ArrayList<String> allString = new ArrayList<>();
            
            while((line = br.readLine())!= null){
                everyLine = line;
                //System.out.println(everyLine);
                allString.add(everyLine);
                String [] splitStr = everyLine.split(",");
                for(Person p : masterPersonList.getPersonDirectory()){
                    if(p.getName().equals(splitStr[0])){
                        UserAccount ua = p.getAccountDirectory().addAccount();
                        ua.setPerson(p);
                        ua.setUserId(splitStr[1]);
                        ua.setPassword(String.valueOf(splitStr[2].hashCode()));
                        ua.setRole(splitStr[3]);
                    }
                        
                }
            }
        }
        catch(IOException e){
            e.printStackTrace();
        }
        int UserAccountCount = business.refershMasterAccountDirectory();
        System.out.println("Config Account Successful.: "+UserAccountCount);
    }
    
    
}
