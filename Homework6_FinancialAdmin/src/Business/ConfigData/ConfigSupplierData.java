/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.ConfigData;

import Business.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author liufulai
 */
public class ConfigSupplierData {
    private Business business;
    private SupplierDirectory supplierDirectory;

    public ConfigSupplierData(Business business) {
        this.business = business;
        this.supplierDirectory = business.getSupplierDirectory();
        
        ReadSupplierCSV();
        ReadProductCSV();
        
        
    }
    
    public void ReadSupplierCSV(){
        File csv = new File("src/Business/ConfigData/Supplier.csv");
        BufferedReader br = null;
        try{
            br = new BufferedReader(new FileReader(csv));
        }
        catch(FileNotFoundException e){
            e.printStackTrace();
        }
        String line = "";
        String everyLine = "";
        try{
            ArrayList<String> allString = new ArrayList<>();
            
                while((line = br.readLine())!= null){
                everyLine = line;
                //System.out.println(everyLine);
                allString.add(everyLine);
                String [] splitStr = everyLine.split(",");
                Supplier s = this.supplierDirectory.addSupplier();
                s.setName(splitStr[0]);
                //System.out.println(p.getName());
            }
        }
        catch(IOException e){
            e.printStackTrace();
        }
        int Count = business.getSupplierDirectory().getSupplierDirectory().size();
        System.out.println("Config Supplier Successful: "+Count);
    }
    
    public void ReadProductCSV(){
        File csv = new File("src/Business/ConfigData/Product.csv");
        BufferedReader br = null;
        try{
            br = new BufferedReader(new FileReader(csv));
        }
        catch(FileNotFoundException e){
            e.printStackTrace();
        }
        String line = "";
        String everyLine = "";
        try{
            ArrayList<String> allString = new ArrayList<>();
            
            while((line = br.readLine())!= null){
                everyLine = line;
                //System.out.println(everyLine);
                allString.add(everyLine);
                String [] splitStr = everyLine.split(",");
                for(Supplier s : this.supplierDirectory.getSupplierDirectory()){
                    if(s.getName().equals(splitStr[0])){
                        Product p  = s.addProduct();
                        p.setProductName(splitStr[1]);
                        p.setTargetPrice(Integer.parseInt(splitStr[2]));
                        p.setDescription(splitStr[3]);
                        p.setAvailNum(Integer.parseInt(splitStr[4]));
                    }
                }
            }
        }
        catch(IOException e){
            e.printStackTrace();
        }
        for(Supplier s : this.supplierDirectory.getSupplierDirectory()){
            int count = s.getProductCatalog().size();
            System.out.println(s.getName()+": "+count);
        }
        System.out.println("Config Product Successful");
    }
    
    
    
}
