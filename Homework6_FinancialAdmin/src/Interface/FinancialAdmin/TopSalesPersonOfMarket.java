/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface.FinancialAdmin;

import Business.*;
import java.awt.CardLayout;
import java.util.ArrayList;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author luoxiao666
 */
public class TopSalesPersonOfMarket extends javax.swing.JPanel {

    /**
     * Creates new form TopSalesPersonOfMarket
     */
    private JPanel Container;
    private Business business;
    private Market market;
    public TopSalesPersonOfMarket(JPanel Container, Business business,Market m) {
        initComponents();
        this.Container = Container;
        this.business = business;
        this.market = m;
        txtMarket.setText(m.getMarketName());
        txtMarket.setEnabled(false);
        ArrayList<Person> personList = AllPerson();
        rank(personList);
        refreshTable(personList);      
        
    }
    
    public ArrayList<Person> AllPerson(){
        ArrayList<Person> personList = new ArrayList<Person>();
        for(Person p: this.business.getMasterPersonList().getPersonDirectory()){
            personList.add(p);
        }
        return personList;
    }
    
    
    public void refreshTable(ArrayList<Person> personList) {
        int rowCount = tblPerson.getRowCount();
        DefaultTableModel model = (DefaultTableModel)tblPerson.getModel();
        for(int i=rowCount-1;i>=0;i--) {
            model.removeRow(i);
        }
        int rank = 1;
        for(Person p : personList){
            Object row[] = new Object[3];
            row[0] = p;
            row[1] = GetPersonProfit(p);
            row[2] = rank;
            rank ++;
            model.addRow(row);
        }
    }
    
    
    public int GetPersonProfit(Person p){
        int Profit = 0;
        for(Customer c : market.getCustomerList().getCustomerList()){
            for(Order o : c.getOrderList().getOrderList()){
                if(o.getPerosn().equals(p)){
                    Profit = Profit + o.GetOrderProfit();
                }
            }
        }
        return Profit;
    }
    
    public void rank(ArrayList<Person> personList){
        int a=0;
        int b=0;
        for(int i=0;i<personList.size()-1;i++){
            for(int j=0;j<personList.size()-1-i;j++){
                a = GetPersonProfit(personList.get(j));
                b = GetPersonProfit(personList.get(j+1));              
                if(a<b){
                    Person aa = new Person();
                    aa=personList.get(j);
                    personList.set(j, personList.get(j+1));
                    personList.set(j+1, aa);
                }
            }
        } 
       
    }   

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        txtMarket = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblPerson = new javax.swing.JTable();
        btnBack = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("宋体", 1, 18)); // NOI18N
        jLabel1.setText("Market Name");

        txtMarket.setFont(new java.awt.Font("宋体", 0, 18)); // NOI18N
        txtMarket.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMarketActionPerformed(evt);
            }
        });

        tblPerson.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Name", "Gap", "Rank"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Integer.class, java.lang.Integer.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblPerson);

        btnBack.setText("Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnBack)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 418, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(64, 64, 64)
                        .addComponent(txtMarket, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(443, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtMarket, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                .addComponent(btnBack)
                .addGap(24, 24, 24))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtMarketActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMarketActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMarketActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
        Container.remove(this);
        CardLayout layout = (CardLayout) Container.getLayout();
        layout.previous(Container);
    }//GEN-LAST:event_btnBackActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblPerson;
    private javax.swing.JTextField txtMarket;
    // End of variables declaration//GEN-END:variables
}
