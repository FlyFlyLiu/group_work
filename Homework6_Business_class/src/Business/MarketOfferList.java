/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author liufulai
 */
public class MarketOfferList {
    private ArrayList<MarketOffer> marketOfferList;

    public MarketOfferList() {
        marketOfferList = new ArrayList<MarketOffer>();
    }

    public ArrayList<MarketOffer> getMarketOfferList() {
        return marketOfferList;
    }

    public void setMarketOfferList(ArrayList<MarketOffer> marketOfferList) {
        this.marketOfferList = marketOfferList;
    }
    
    public MarketOffer addMarketOffer(){
        MarketOffer mo = new MarketOffer();
        this.marketOfferList.add(mo);
        return mo;
    }
    
    public void removeMarketOffer(MarketOffer mo){
        this.marketOfferList.remove(mo);
    }
    
    public MarketOffer searchMarketOffer(String prodName, String MarketName){
        for(MarketOffer mo : this.marketOfferList){
            if(mo.getProduct().getProductName().equalsIgnoreCase(prodName)){
                if(mo.getMarket().getMarketName().equalsIgnoreCase(MarketName)){
                    return mo;
                }
            }
        }
        return null;
    }
    
    public boolean searchByProduct(String prodName){
        for(MarketOffer mo : this.marketOfferList){
            if(mo.getProduct().getProductName().equalsIgnoreCase(prodName)){
                return true;
            }
        }
        return false;
    }
    
    public boolean searchByMarket(String marketName){
        for(MarketOffer mo : this.marketOfferList){
            if(mo.getMarket().getMarketName().equalsIgnoreCase(marketName)){
                return true;
            }
        }
        return false;
    }
    
    
    
    
}
